#!/bin/bash

VERSION="20200622"
OUTPUT_DIR="./data/${VERSION}"


declare -a models=( \
    # "*" \
    "IPSL-CM6A-LR" \
    "CESM2*" \
    "NorESM*" \
)

declare -a variables=( \
    "tas" \
    "tos" \
    "rsdt" \
    "rlut" \
    "rsut" \
    "rndt" \
    "hfds" \
)

declare -a experiments=( \
    "abrupt-2xCO2" \
    "abrupt-0p5xCO2" \
    "1pctCO2" \
    "abrupt-4xCO2" \
    "historical" \
    "ssp245" \
)


for model in "${models[@]}"
do
    echo "Model: $model"

    include=( )

    for variable in "${variables[@]}"
    do

        for experiment in "${experiments[@]}"
        do

            include_str="*_${variable}_*_${model}_${experiment}_*"
            include+=( --include "${include_str}" )

        done

    done

    echo "${include[@]}"

    aws s3 sync \
        "s3://cmip6-calibration-data/CMIP6/${VERSION}/ascii/mag" \
        "${OUTPUT_DIR}" \
        --exclude "*" \
        "${include[@]}"

done
