#!/usr/bin/env bash
#
# Perform a release
# This script is a work in progress
#
# Should be run from the root of the git directory
#

# Log commands
set -x

RELEASE=${1:-$(date +"%Y%m%d")}
TARGET=CMIP6
FLAGS="--name $RELEASE --target $TARGET"

SCRIPT_DIR=$(dirname "$BASH_SOURCE")
RELEASE_DIR=$SCRIPT_DIR/../data/$TARGET

echo "Creating new release $RELEASE"
python $SCRIPT_DIR/manage_data.py release $FLAGS

# Add the crunched NC files
# $SCRIPT_DIR/manage_data.py add --name $RELEASE -p nc $RELEASE_DIR/nc

# Stich and wrangle the CMIP6 MAG files
# Still to do - using local files

python $SCRIPT_DIR/manage_data.py add $FLAGS -p ascii/mag $RELEASE_DIR/ascii/mag
python $SCRIPT_DIR/manage_data.py add $FLAGS -p zombimon/mag $RELEASE_DIR/zombimon/mag

# Create database and JSON files
# The subset file is for the CMIP6 data website
echo "Creating databases"
python $SCRIPT_DIR/create_database.py $FLAGS --dedup \
    ascii/mag/monthly \
    ascii/mag/average-year-mid-year \
    database-CMIP6-subset.csv.gz
python $SCRIPT_DIR/create_database.py $FLAGS --dedup \
    ascii/mag/monthly \
    ascii/mag/average-year-mid-year \
    ascii/mag/average-year-start-year \
    ascii/mag/point-start-year \
    database-CMIP6.csv.gz

# Remove references to bad files
python nix_files.py --database $RELEASE_DIR/ascii/database/database-CMIP6-subset.csv
python nix_files.py --database $RELEASE_DIR/ascii/database/database-CMIP6.csv.gz

#python $SCRIPT_DIR/create_json.py $FLAGS
python $SCRIPT_DIR/create_nc.py $FLAGS

# Run zombimon processing
#bash run-zombimon.sh

#python $SCRIPT_DIR/create_database.py $FLAGS --dedup \
#    zombimon \
#    database-CMIP6-zombimon.csv.gz

echo "Uploading data"
python $SCRIPT_DIR/manage_data.py add $FLAGS -p bin/nc $RELEASE_DIR/bin/nc
python $SCRIPT_DIR/manage_data.py add $FLAGS -p database $RELEASE_DIR/database
