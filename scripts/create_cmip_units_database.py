#!/bin/env python
"""
Converts CMIP units into a single database
"""
import click
import pandas as pd
from tqdm import tqdm


def _load_cmip_units_file(fp):
    xl = pd.ExcelFile(fp)
    sheet_names = xl.sheet_names[1:]  # first sheet is just Notes - ignore that one.

    df = pd.DataFrame({"Long name": [], "units": [], "Variable Name": []})

    click.echo("Combining {} CMIP unit sheets".format(len(sheet_names)))
    for sheet_name in tqdm(sheet_names, desc="Sheets", leave=False):
        first_sheet = pd.read_excel(
            fp,
            sheet_name=sheet_name
        )[["Long name", "units", "Variable Name"]]
        df = pd.concat([df, first_sheet], axis=0, sort=True)

    df.rename(
        columns={"Long name": "long_name", "Variable Name": "variable_id"}, inplace=True
    )

    df = df[["long_name", "units", "variable_id"]]

    return df


@click.command(context_settings={"help_option_names": ["-h", "--help"]})
@click.argument("src", type=click.Path(exists=True, dir_okay=False, resolve_path=True, readable=True))
@click.argument("dst", type=click.Path(dir_okay=False, resolve_path=True, writable=True))
def convert_cmip_xlsx(src, dst):
    r"""
    Convert CMIP units xlsx specified by ``src`` to a single csv in ``dst``
    """
    cmip_units = _load_cmip_units_file(src)
    cmip_units.to_csv(dst, index=False)


if __name__ == "__main__":
    convert_cmip_xlsx()
