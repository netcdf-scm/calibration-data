#!/bin/env python
"""
Converts the MAG files to NC files for ingestion by the website

creates the cmip5/bin/nc and CMIP6/bin/nc archives
"""
import json
from os.path import isfile, join, dirname, exists
import os
import multiprocessing

import click
import pandas as pd
from pymagicc.io import MAGICCData
from tqdm import tqdm


def ensure_dir_exists(fname):
    d = dirname(fname)
    if not exists(d):
        try:
            os.makedirs(d)
        except FileExistsError:
            # There can be a race condition creating folder
            pass


def process_file(inp):
    row, fname = inp
    if fname.endswith(".nc"):
        return

    pt2 = fname.replace("MAG", "nc").replace("ascii/mag", "bin/nc")
    if isfile(pt2):
        # already converted
        return

    if not isfile(fname) or ".MAG" not in fname:
        raise ValueError(
            "How did non-existent, non-MAG file get into database?: {}".format(fname)
        )

    md = MAGICCData(fname)
    md.metadata = row.to_dict()

    meta_to_copy = [
        ("institution_id", "institution_id"),
        ("source_id", "climate_model"),
        ("experiment_id", "scenario"),
        ("member_id", "member_id"),
        ("normalisation_method", "normalisation_method")
    ]

    for src, target in meta_to_copy:
        md[target] = row[src]

    ensure_dir_exists(pt2)
    md.to_nc(pt2)


@click.command()
@click.option("--name", "-n", help="Release name", required=True)
@click.option(
    "--target",
    "-t",
    type=click.Choice(["cmip5", "CMIP6"], case_sensitive=False),
    required=True,
)
def create_nc(name, target):
    release_dir = join(dirname(__file__), "..", "data", target)

    database_file = join(
        release_dir, "database", "database-{}-subset.csv".format(target)
    )
    db = pd.read_csv(database_file)

    del db["normalised"]

    with multiprocessing.Pool(32) as pool:
        for _ in tqdm(
            pool.imap(
                process_file,
                [(row, join(release_dir, row.fullpath)) for _, row in db.iterrows()],
                chunksize=20,
            ),
            total=len(db["fullpath"]),
            unit="file",
        ):
            pass
        pool.close()
        pool.join()


if __name__ == "__main__":
    create_nc()
