from os import makedirs
from os.path import basename, join, exists
from shutil import copyfile
from concurrent.futures import ProcessPoolExecutor, as_completed

import click
import pandas as pd
from tqdm import tqdm


def make_flat_dirs(target):
    def _m(d):
        if not exists(d):
            makedirs(d)

    _m(join(target, "flat", "average-year-mid-year"))
    _m(join(target, "flat", "average-year-start-year"))
    _m(join(target, "flat", "point-start-year"))


def run_parallel(fn, fn_args):
    with ProcessPoolExecutor(max_workers=20) as executor:

        futures = [executor.submit(fn, *a) for a in fn_args]

        for future in tqdm(
            as_completed(futures), total=len(futures)
        ):
            try:
                future.result()
            except Exception as exc:
                print("Exception: %s" % (exc))

@click.command()
@click.option(
    "--database",
    type=click.Path(exists=True, dir_okay=False, resolve_path=True, readable=True)
)
@click.argument("src", type=click.Path(file_okay=False, resolve_path=True, readable=True))
def main(database, src):
    metadata = pd.read_csv(database)
    metadata = metadata[metadata.timeseriestype != "monthly"]

    make_flat_dirs(src)

    fn_args = []
    for _, row in tqdm(metadata.iterrows()):
        fname = join(src, row.fullpath)
        target_fname = join(src, "flat", row.timeseriestype, basename(row.fullpath))
        fn_args.append((fname, target_fname))

    run_parallel(copyfile, fn_args)


if __name__ == "__main__":
    main()
