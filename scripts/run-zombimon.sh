#!/bin/bash
#
# Run the zombimon processing scripts
# Creates additional derived variables for CMIP6 data
#
# * Needs to be run after the database files are created

echo "Starting processing zombimon"

SCRIPT_DIR=$(dirname "$BASH_SOURCE")
NOTEBOOK_DIR="$SCRIPT_DIR/../notebooks/zombimon"

echo "Cleaning dir"
rm -r $SCRIPT_DIR/../data/CMIP6/zombimon

# Runs notebooks matching the pattern in alphabetical order
for nb in $NOTEBOOK_DIR/*-zombimon-*.ipynb; do
  echo "running $nb"
  jupyter nbconvert \
    --to notebook \
    --execute \
    --ExecutePreprocessor.timeout=60000 \
    "$nb"
done


# Crunch the CO2 data - preprocessed using 03-zombimon-co2.ipynb
bash $SCRIPT_DIR/run-process.sh $SCRIPT_DIR/config/cmip6_zombimon.json

echo "Zombimon complete"