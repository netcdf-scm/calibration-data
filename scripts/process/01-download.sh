#!/bin/bash
#
#  Download new NetCDF files using ESGF Scraper
#

source ${SCRIPT_DIR}/process/funcs.sh

GRID_LABEL="gr,gn"
FREQUENCY="mon,fx"

EXTRA_VARIABLES=$(get_config '.download.extra_variables') || exit 1
EXTRA_EXPERIMENTS=$(get_config '.download.extra_experiments') || exit 1

echo "Starting to download"

esgf -l INFO search \
  -f variable_id="${EXTRA_VARIABLES},${VARIABLE_ID}" \
  -f grid_label=${GRID_LABEL} \
  -f experiment_id="${EXTRA_EXPERIMENTS},${EXPERIMENT_ID}" \
  -f frequency=${FREQUENCY} \
  --add  && esgf -l INFO download

echo "Download completed"
