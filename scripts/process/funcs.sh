common_config=$(cat "${SCRIPT_DIR}/config/common.json")

get_config() {
  # Get the value of a flag
  # Check CONFIG and then check the common config if it isn't found
  config_flag=$(echo "${CONFIG}" | jq -r "$1")

  if [ "$config_flag" = "null" ]; then
    config_flag=$(echo "$common_config" | jq -r "$1")
  fi

  if [ "$config_flag" = "null" ]; then
    echo "Could not find config for $1" >&2
    exit 1
  fi

  if [ "$2" = "true" ]; then
    res=$(echo "$config_flag" | jq -r 'join(",")')
    if [ $? -ne 0 ]
    then
        echo "Could not parse $config_flag as a list" >&2
        exit 1
    fi
    config_flag=$res
  else
    # Convert any env variables
    config_flag=$(eval echo $config_flag)
  fi
  echo "$config_flag"
}

get_stitch_config() {
  if [ $(get_config ".convert_units") = true ]; then
    echo "--target-units-specs process/stitch-units-specs.csv"
  fi
}


get_overlap() {
  overlap_vars=""

  if [ "$2" == ".*" ]
  then
    overlap_vars=$1
  else
    for v in ${1//,/ }; do
      for i in ${2//,/ }; do
        if [ "$v" == "$i" ]; then
          overlap_vars+="$v,"
        fi
      done
    done
  fi

  if [ ${#overlap_vars} -gt 0 ]; then
    echo "${overlap_vars::-1}"
  fi
}
