#!/bin/bash
#
#   Normalising
#

source ${SCRIPT_DIR}/process/funcs.sh

AVERAGE_START_YEAR_VARIABLES=$(get_config '.average_start_year_variables' true) || exit 1
AVERAGE_MID_YEAR_VARIABLES=$(get_config '.average_mid_year_variables' true) || exit 1
POINT_START_YEAR_VARIABLES=$(get_config '.point_start_year_variables' true) || exit 1

# Only run variables if they are specifed in variables
AVERAGE_START_YEAR_VARIABLES=$(get_overlap $VARIABLE_ID $AVERAGE_START_YEAR_VARIABLES)
AVERAGE_MID_YEAR_VARIABLES=$(get_overlap $VARIABLE_ID $AVERAGE_MID_YEAR_VARIABLES)
POINT_START_YEAR_VARIABLES=$(get_overlap $VARIABLE_ID $POINT_START_YEAR_VARIABLES)

echo "Normalising AVERAGE_START_YEAR_VARIABLES=$AVERAGE_START_YEAR_VARIABLES AVERAGE_MID_YEAR_VARIABLES=$AVERAGE_MID_YEAR_VARIABLES POINT_START_YEAR_VARIABLES=$POINT_START_YEAR_VARIABLES"


STITCH_NORM_METHOD="21-yr-running-mean"
if [ "${TARGET}" = "CMIP6" ]; then
  STITCH_REGEXP="/(${SOURCE_ID//,/|})/(${EXPERIMENT_ID//,/|})/(${VARIANT_LABEL//,/|})/"
else
  STITCH_REGEXP="/(${EXPERIMENT_ID//,/|})/"
fi

EXTRA_CONFIG=$(get_stitch_config)
RUN_MONTHLY=$(get_config '.run_monthly') || exit 1

echo "Starting to normalise"

if [ "${RUN_MONTHLY}" = true ]; then
  netcdf-scm stitch \
    ${CRUNCH_DIR_SUBDIR} \
    "${STITCH_OUTPUT_DIR_ROOT}/mag/monthly" \
    "${CONTACT}" \
    --drs "${DRS}" \
    --out-format "mag-files" \
    --number-workers ${STITCH_NUMBER_WORKERS} \
    --regexp "^(?!.*(piControl)).*${STITCH_REGEXP}.*/(${VARIABLE_ID//,/|})/.*$" \
    --normalise ${STITCH_NORM_METHOD} \
    --prefix "NORMED" $EXTRA_CONFIG
fi

netcdf-scm stitch \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/point-start-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-point-start-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^(?!.*(piControl)).*${STITCH_REGEXP}.*/(${POINT_START_YEAR_VARIABLES//,/|})/.*$" \
  --normalise ${STITCH_NORM_METHOD} \
  --prefix "NORMED" $EXTRA_CONFIG


# mag files average year start of year normalised
netcdf-scm stitch \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/average-year-start-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-average-year-start-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^(?!.*(piControl)).*${STITCH_REGEXP}.*/(${AVERAGE_START_YEAR_VARIABLES//,/|})/.*$" \
  --normalise ${STITCH_NORM_METHOD} \
  --prefix "NORMED" $EXTRA_CONFIG


# mag files average year middle of year normalised
netcdf-scm stitch \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/average-year-mid-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-average-year-mid-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^(?!.*(piControl)).*${STITCH_REGEXP}.*/(${AVERAGE_MID_YEAR_VARIABLES//,/|})/.*$" \
  --normalise ${STITCH_NORM_METHOD} \
  --prefix "NORMED" $EXTRA_CONFIG

# mag files middle of year normalised for tas for interest
netcdf-scm stitch \
  ${CRUNCH_DIR_SUBDIR} \
  "${STITCH_OUTPUT_DIR_ROOT}/mag/average-year-mid-year" \
  "${CONTACT}" \
  --drs "${DRS}" \
  --out-format "mag-files-average-year-mid-year" \
  --number-workers ${STITCH_NUMBER_WORKERS} \
  --regexp "^(?!.*(piControl)).*${STITCH_REGEXP}.*/tas.*/.*$" \
  --normalise "30-yr-running-mean" \
  --prefix "NORMED30"

echo "normalisation complete"
