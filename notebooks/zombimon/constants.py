from os.path import join, dirname

CMIP6_DIR = "/data/cmip6"
REPO_ROOT = join(dirname(__file__), "..", "..")

RELEASE_DIR = join(REPO_ROOT, "data", "CMIP6")
ZOMBIMON_DIR = join(RELEASE_DIR, "zombimon")
DATABASE_DIR = join(RELEASE_DIR, "database")
